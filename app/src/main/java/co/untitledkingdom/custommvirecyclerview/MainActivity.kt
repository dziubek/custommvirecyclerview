package co.untitledkingdom.custommvirecyclerview

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.os.PersistableBundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import co.untitledkingdom.custommvirecyclerview.list.PersonItemViewModel
import co.untitledkingdom.custommvirecyclerview.list.PersonsAdapter
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject
import kotlinx.android.synthetic.main.activity_main.recyclerView

class MainActivity : AppCompatActivity(), MainView {

    private val fetchTriggerSubject: Subject<Boolean> = PublishSubject.create()

    private val fetchDataKey = "fetchData"

    private lateinit var mainViewModel: MainViewModel
    private lateinit var personItemViewModel: PersonItemViewModel

    private lateinit var personAdapter: PersonsAdapter

    private var fetchData: Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mainViewModel = ViewModelProviders.of(this)[MainViewModel::class.java]
        personItemViewModel = ViewModelProviders.of(this)[PersonItemViewModel::class.java]

        savedInstanceState?.let {
            fetchData = it.getBoolean(fetchDataKey)
        }

        personAdapter = PersonsAdapter(personItemViewModel)

        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = personAdapter

    }

    override fun onSaveInstanceState(outState: Bundle, outPersistentState: PersistableBundle?) {
        super.onSaveInstanceState(outState, outPersistentState)
        outState.putBoolean(fetchDataKey, fetchData)
    }

    override fun onStart() {
        super.onStart()
        mainViewModel.bind(this)
        fetchTriggerSubject.onNext(fetchData)
    }

    override fun onStop() {
        super.onStop()
        fetchData = false
        mainViewModel.unbind()
        personItemViewModel.unbindAll()
    }

    override fun render(mainViewState: MainViewState) {
        personAdapter.setPersonStateList(mainViewState.personItemViewStateList)
    }

    override fun emitDataFetchTrigger(): Observable<Boolean> = fetchTriggerSubject
}