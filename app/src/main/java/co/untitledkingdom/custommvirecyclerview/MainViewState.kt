package co.untitledkingdom.custommvirecyclerview

import co.untitledkingdom.custommvirecyclerview.list.PersonItemViewState

data class MainViewState(val personItemViewStateList: List<PersonItemViewState>)