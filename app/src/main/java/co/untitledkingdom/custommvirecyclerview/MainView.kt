package co.untitledkingdom.custommvirecyclerview

import co.untitledkingdom.custommvirecyclerview.MainViewState
import io.reactivex.Observable

interface MainView {

    fun emitDataFetchTrigger(): Observable<Boolean>

    fun render(mainViewState: MainViewState)
}