package co.untitledkingdom.custommvirecyclerview.models

data class Answer(val name: String, val answer: String)