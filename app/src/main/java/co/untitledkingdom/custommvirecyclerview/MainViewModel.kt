package co.untitledkingdom.custommvirecyclerview

import android.arch.lifecycle.ViewModel
import co.untitledkingdom.custommvirecyclerview.list.PersonItemViewState
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.BehaviorSubject

class MainViewModel(private val mainInteractor: MainInteractor = MainInteractor()) : ViewModel() {

    private val compositeDisposable = CompositeDisposable()
    private val stateSubject = BehaviorSubject.create<MainViewState>()

    fun bind(mainView: MainView) {
        val fetchDataObservable = mainView.emitDataFetchTrigger()
                .filter({ it })
                .flatMap { mainInteractor.fetchData() }
                .map {
                    val itemViewStateList = arrayListOf<PersonItemViewState>()
                    it.mapTo(itemViewStateList) { PersonItemViewState(person = it) }
                    MainViewState(itemViewStateList)
                }
                .subscribeWith(stateSubject)

        compositeDisposable.add(fetchDataObservable.subscribe({ mainView.render(it) }))
    }

    fun unbind() {
        compositeDisposable.clear()
    }
}