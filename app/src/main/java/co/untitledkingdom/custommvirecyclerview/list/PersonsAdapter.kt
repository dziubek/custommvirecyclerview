package co.untitledkingdom.custommvirecyclerview.list

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import co.untitledkingdom.custommvirecyclerview.R

class PersonsAdapter(private val personItemViewModel: PersonItemViewModel) : RecyclerView.Adapter<PersonItemViewHolder>() {

    override fun getItemCount(): Int = personItemViewModel.getItemCount()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PersonItemViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_person, parent, false)
        return PersonItemViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: PersonItemViewHolder, position: Int) {
        personItemViewModel.bind(holder, position)
    }

    fun setPersonStateList(personItemStateList: List<PersonItemViewState>) {
        personItemViewModel.setPersonStateList(personItemStateList)
        notifyDataSetChanged()
    }
}