package co.untitledkingdom.custommvirecyclerview.list

import co.untitledkingdom.custommvirecyclerview.models.Person

data class PersonItemViewState(val progress: Boolean = false, val success: Boolean = false,
                               val typedInput: String = "", val person: Person = Person())