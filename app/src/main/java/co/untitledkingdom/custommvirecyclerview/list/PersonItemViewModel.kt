package co.untitledkingdom.custommvirecyclerview.list

import android.arch.lifecycle.ViewModel
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.BehaviorSubject

class PersonItemViewModel(private val personItemInteractor: PersonItemInteractor = PersonItemInteractor()) : ViewModel() {

    private val stateSubjectsList = arrayListOf<BehaviorSubject<PersonItemViewState>>()
    private val disposableList = arrayListOf<Disposable>()

    fun bind(personItemView: PersonItemView, position: Int) {
        val stateSubject = stateSubjectsList[position]

        val buttonObservable = personItemView.emitButtonClick()
                .flatMap { personItemInteractor.processInput("nothing").startWith(PartialPersonItemViewState.ProgressState()) }

        val intentObservable = buttonObservable.scan(stateSubject.value, this::reduce)
                .subscribeWith(stateSubject)

        disposableList.add(intentObservable.subscribe({ personItemView.render(it) }))
    }

    private fun reduce(previousState: PersonItemViewState, partialState: PartialPersonItemViewState)
            : PersonItemViewState {
        return when (partialState) {
            is PartialPersonItemViewState.ProgressState -> previousState.copy(progress = true, success = false)
            is PartialPersonItemViewState.SuccessState -> previousState.copy(progress = false, success = true)
        }
    }

    fun unbind(position: Int) {
        disposableList[position].dispose()
        disposableList.removeAt(position)
    }

    fun unbindAll() {
        for (disposable in disposableList) {
            disposable.dispose()
        }
        disposableList.clear()
    }

    fun setPersonStateList(personItemStateList: List<PersonItemViewState>) {
        if (stateSubjectsList.size == 0) {
            personItemStateList.mapTo(stateSubjectsList) { BehaviorSubject.createDefault(it) }
        }
    }

    fun getItemCount(): Int = stateSubjectsList.size
}