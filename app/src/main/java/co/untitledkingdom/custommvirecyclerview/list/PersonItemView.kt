package co.untitledkingdom.custommvirecyclerview.list

import io.reactivex.Observable

interface PersonItemView {

    fun render(personItemViewState: PersonItemViewState)

    fun emitButtonClick(): Observable<Boolean>
}