package co.untitledkingdom.custommvirecyclerview.list

sealed class PartialPersonItemViewState {

    class SuccessState : PartialPersonItemViewState()

    class ProgressState : PartialPersonItemViewState()
}