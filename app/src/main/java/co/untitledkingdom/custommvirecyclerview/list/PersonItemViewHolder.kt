package co.untitledkingdom.custommvirecyclerview.list

import android.support.v7.widget.RecyclerView
import android.view.View
import com.jakewharton.rxbinding2.view.RxView
import io.reactivex.Observable
import kotlinx.android.synthetic.main.item_person.view.dummyButton
import kotlinx.android.synthetic.main.item_person.view.inputEditText
import kotlinx.android.synthetic.main.item_person.view.nameTextView
import kotlinx.android.synthetic.main.item_person.view.progressBar
import kotlinx.android.synthetic.main.item_person.view.successTextView

class PersonItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), PersonItemView {

    lateinit var personItemViewState: PersonItemViewState

    override fun render(personItemViewState: PersonItemViewState) {
        this.personItemViewState = personItemViewState
        clearView(itemView)
        with(itemView) {
            nameTextView.text = personItemViewState.person.name
            inputEditText.setText(personItemViewState.typedInput)
        }

        with(itemView) {
            if (personItemViewState.progress) {
                progressBar.visibility = View.VISIBLE
                successTextView.visibility = View.GONE
            }

            if (personItemViewState.success) {
                progressBar.visibility = View.GONE
                successTextView.visibility = View.VISIBLE
            }
        }
    }

    private fun clearView(itemView: View) {
        with(itemView) {
            progressBar.visibility = View.GONE
            successTextView.visibility = View.GONE
        }
    }

    override fun emitButtonClick(): Observable<Boolean> {
        return RxView.clicks(itemView.dummyButton).map { true }
    }
}