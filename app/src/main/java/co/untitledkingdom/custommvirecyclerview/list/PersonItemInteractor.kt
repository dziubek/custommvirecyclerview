package co.untitledkingdom.custommvirecyclerview.list

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import java.util.concurrent.TimeUnit

class PersonItemInteractor {

    fun processInput(input: String): Observable<PartialPersonItemViewState> {
        return Observable.timer(3000, TimeUnit.MILLISECONDS)
                .map { PartialPersonItemViewState.SuccessState() }
                .observeOn(AndroidSchedulers.mainThread()) as Observable<PartialPersonItemViewState>
    }
}