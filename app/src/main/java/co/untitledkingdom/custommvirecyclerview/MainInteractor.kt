package co.untitledkingdom.custommvirecyclerview

import co.untitledkingdom.custommvirecyclerview.models.Person
import io.reactivex.Observable

class MainInteractor {

    fun fetchData(): Observable<List<Person>> {
        return Observable.just(listOf(Person("Mateusz"),
                Person("Filip"),
                Person("Paweł"),
                Person("Łukasz"),
                Person("Grzegorz"),
                Person("Maria"),
                Person("Marek"),
                Person("Piotr"),
                Person("Leszek"),
                Person("Marcel")))
    }
}